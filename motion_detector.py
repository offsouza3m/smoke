import cv2
import numpy as np
import ast
from time import time 
from itertools import compress

class MotionDetector:
    """ Verifica se teve movimento na área selecionada de uma câmera"""
    def __init__(self, threshold = 0.001, verbose = False): 
        self.last_frame = {}
        self.frame = None
        self.verbose = verbose     
        self.threshold = threshold 
        self.det_moving = dict()

    def update_last_frame(self, frame, cam):
        self.last_frame[cam] = frame

    def get_roi_from_points(self,frame, points):    
        ''' Realiza o crop da região de interesse na imagem
        :param frame: array -> imagem
        :param points: list -> região de interesse (x,y,w,h)
        '''    
        x,y,w,h = points
        croped = frame[y:y+h, x:x+w].copy()
        return croped   

    def detector(self, camera, detections): 
        '''
        Verifica se houve movimento durante 1 segundo e retorna as regiões que tiveram movimento
        :param camera: class CameraBufferCleanerThread 
        :param detections: list -> lista de detecções
        '''
        t0 = time()
        while time() - t0 < 1:
            print('************')
            frame = camera.last_frame
            for n_det, dets in enumerate(detections):
                bbox = dets["xywh"]
                roi = self.get_roi_from_points(frame,bbox)
                if len(roi) <=0 or len(frame) <=0: continue
                if n_det in self.last_frame:                   
                    diff = cv2.absdiff(self.last_frame[n_det], roi) 
                    gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
                    _, thresh = cv2.threshold(gray, 20, 255, cv2.THRESH_BINARY)
                    dilated = cv2.dilate(thresh, None, iterations=2)        
                    contours, _ = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)      
                    moving = self.is_moving(contours, n_det)
                    self.show(n_det, thresh) if self.verbose else False
                    if moving: self.det_moving[n_det] = True          
                else: 
                    self.update_last_frame(roi, n_det)
                    self.det_moving[n_det] = False

        return list(compress(detections, list(self.det_moving.values())))
               

    def is_moving (self,  contours, cam):   
        '''
        Verifica se área da região do movimento é maior que o limite configurado
        Retorna True se área for maior que o limite
        '''       
        if len(contours) > 10: return True
        for contour in contours:
                (x, y, w, h) = cv2.boundingRect(contour)                
                print(cv2.contourArea(contour) / np.prod(self.last_frame[cam].shape[:2]) )
                if cv2.contourArea(contour) / np.prod(self.last_frame[cam].shape[:2]) > self.threshold:   
                    return True       
        return False

    def show(self, cam, thresh ):
        cv2.imshow('Motion Detector:%s'%cam, self.last_frame[cam])
        cv2.imshow('Difference Frame:%s'%cam, thresh)        
        if cv2.waitKey(40) == 27:
                self.verbose = False
                

if __name__ == "__main__":

    cap = cv2.VideoCapture(0)   
    motion = MotionDetector(verbose=True)
    
    while cap.isOpened():

        ret, frame = cap.read()
        is_moving = motion.detector(frame, 0)
        print("Is Moving ") if is_moving else print("Is not Moving")
        if not motion.verbose:
            break 

    cap.release()
    cv2.destroyAllWindows()