# API - Detecção de Fumaça #



A API foi  desenvolvido em python capaz de realizar detecção de fumaça através do stream de câmeras via rtsp. 

Toda a comunicação com a API é via REST usando os métodos `/GET` e `/POST`.



## Executar API ( Windows 10 -11)

Para executar a API basta fazer o download do arquivo `smoke.rar` que está disponível no drive.

Uma vez baixado o arquivo `smoke.rar` basta extrai-ló para algum local de sua preferência. Após a extração, deve encontrar na uma pasta chamada `smoke`, dentro da pasta você irá encontrar um arquivo chamado `smoke.exe`. Para executar a aplicação basta dar um duplo clique no arquivo `smoke.exe`.

Após alguns segundos irá aparecer uma interface com a imagem abaixo:

![gui](./images/gui.jpg)

Quando sistema estiver *ONLINE*, significa que o modelo de inteligência artificial foi carregado com sucesso e está pronto para uso.

## Gerar nova versão

Ao fazer alguma modificação no sistema para gerar nova versão do executável você precisa ter um ambiente com **python3** instalado e instalar os pacotes necessários.

Para instalar todos os pacotes necessários, entre na **pasta principal**, onde contém o arquivo `requirements.txt`  e execute:

`pip3 install -r requirements.txt`

Ainda no diretorio principal, irá encontrar o arquivo `smoke.spec` e execute o comando abaixo:

`pyinstaller smoke.spec -y`

Uma nova versão irá gerada na pasta `dist/smoke`

## Dockerfile 

Caso queira usar docker, basta fazer o build da imagem usando o `Dockerfile`.

## Obs

Caso for executar em um ambiente sem interface gráfica, desabilite interface gráfica da aplicação, para isso basta editar o campo `gui` do arquivo `config.json` e colocar o valor `false`

## Arquivos

### smoke.py

Esse é o arquivo principal da aplicação. Nesse arquivo é onde é criada a API REST em que o Sysguára consome e também onde é criado a interface gráfica da aplicação.

### detector.py

Esse arquivo contém a classe SmokeDetector que possui as funções de carregar o modelo e fazer a predição.

### flask_routes.py

Esse arquivo contém as rotas da API REST para comunição com o Sysguara.

### motion_detector.py

Contém a classe que realiza detecção de movimento na região onde ocorreu uma detecção.

### gui_interface.py

Contém classe que cria e atualizar a interface gráfica.

### cameras_control.py

Faz o controle das câmeras adicionadas armazenando  as informações de cada.

### cameraBuffer.py

Abre a conecção com câmera e atualiza os frames em tempo real

### cameraBuffer.py

Leitura e escrita das configurações do arquivo config.json

### smoke.spec

Arquivo para criar o executável para windows 10.

### requirements.txt

Contem todas as bibliotecas necessárias para executar aplicação usando python.

### log.py

Gera o arquivo de log, para acompanhar o  funcionamento do sistema.

### *Pasta* yolov5

Contém todos os arquivo e funções necessárias para executar o modelo de detecção de objeto Yolov5.

### *Pasta* model

Contem o modelo treinado para detecção de fumaça `best.pt` e um modelo pré-treinado da yolov5 `yolov5s.pt`. 

### *Pasta* logs

Onde exite o arquivo de log do sistema `smoke.log`.

## Funcionalidades da API



### Ativar detecção de fumaça em uma câmera

​	**`POST`** *https://localhost:5004/activate_camera*

```
{   
	"id": 1,
	"ip":"192.168.2.10",
	"user": "com3telecom" ,
	"password":	"Com328082019",
	'port':554
}
```

#### 	*Response*

​		**Sucess**

```
{
	"code": 0,
	"result": "camera conected"
}

```

​		**Failure**

```
{
	"code": 1,
	"result": "camera not conected"
}

```

------



### Desativar detecção de fumaça em uma câmera

​	**`POST`** *https://localhost:5004/deactivate_camera*

```
{  
	"id": 1
}
```

#### 	*Response*

​		**Sucess**

```
{	
	"code": 0,
	"result": "camera removed"
}

```

​		**Failure**

```
{
	"code": 1,
	"result": "camera id not find"

}

```

------

### Verificar fumaça em um câmera

​	**`POST`** *https://localhost:5004/verify*

```
{  
	"id": 1,
	"width":640,
	"heigth":640,
}
```

#### 	*Response*

​		**Sucess - Detectou fumaça**

```
{	
	{
    "alert": true,
    "dets": [
        [
            [
                0,
                231,
                187,
                204
            ],
            0.4165123403072357
        ]
    ],
    "time": "02/06/2022, 19:06:33"
}
}

```

​		**Sucess - Não detectou Fumaça**

```
{	
	{
    "alert": false,
    "dets": null,
    "time": null
}
}

```



​	

------



### Configurações

​	**conf :** é a confiabilidade da detecção - Ex: se a conf for 0.5 o sistema irá enviar "alert":True, somente se a IA tiver uma 	certeza 	de detecção acima de 0.5 em um escala de 0 a 1 - em que 1 é o maximo de certeza.

​	**verify_frames:** é a quantidade de frames que irão ser detectados por requisição (/verify). A quantidade irá impactar no tempo 	de resposta, quanto maior, mais lento.

​	**max_det:** é a quantidade objetos que irão ser retornados pela IA. Ex: caso o  `max_det =3`, se o sistema encontrar 4 focos de fumaça irá mostrar somente 3(os de maiores confiança) na tela. Valor muito alto pode ficar estranho  quando for exibido na tela. ***Padrão=3***

​	**`	POST`** *https://localhost:5004/setings*

```
{  
	"conf":0.4,
	"verify_frames":1,
	"max_det": 3
}
```

#### 	*Response*

​		**Sucess -**

```
"update setings: Confiabilidade: 0.4, det_per_request: 1, Max Detections: 3"
```



------

### Ver detecções pela IA 

​	Usado principalmente para testes, ao fazer requisição para rota `/show,` quando a rota `/verify` for ativada irá abrir uma janela no sistema IA, mostrando todas as detecções. Rota `/noshow` desabilita.

​	**`	GET`** *https://localhost:5004/show*

​	**`	GET`** *https://localhost:5004/noshow*

------

