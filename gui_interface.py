import PySimpleGUI as sg

class GUI():
    """ Cria e atualiza a interface gráfica"""
    def __init__(self):
        self.layout = [  [sg.Text(size=(50,1), background_color='red3', font='Arial 12', justification='center',text_color="black", key='-STATUS-')],
                [sg.Text("Câmeras Conectadas: ", size=(20,1), text_color="black", background_color='white'), sg.Listbox(values=[],size=(30, 4),key='-CAM-')]
                 ]
        self.window = sg.Window('Detector de Fumaça', self.layout, size=(500,150), margins=(50,25),background_color='white',  location=(0,0), finalize=True,)
        
    def model_online(self,cameras):
        self.window['-STATUS-'].update("ONLINE", background_color='green1')
        self.window['-CAM-'].update(values=list(cameras.keys()))

    def model_offline(self):
        self.window['-STATUS-'].update("OFFLINE")
        self.window['-CAM-'].update(values=[], )
    def check_close(self,event):
        if event == sg.WIN_CLOSED: return True
        else: return False
    def close(self):
        self.window.close()
        import os
        import signal
        sig = getattr(signal, "SIGKILL", signal.SIGTERM)
        os.kill(os.getpid(), sig)        
