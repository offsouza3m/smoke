import string
from threading import Thread
from time import sleep

from detector import SmokeDetector
from config import check_config_file
from cameras_control import Cameras_control
from flask_routes import tracking_app
from gui_interface import GUI
from log import get_logger

logger = get_logger("smoke")

def start_flask(ip_server):
    """ 
    Inicia servidor Flask na porta 5004
    :param ip_server: string 
    """
    tracking_thread = Thread(target=tracking_app.run, kwargs=dict(host=ip_server, port=5004))
    tracking_thread.daemon = False
    tracking_thread.start()

def finish_app(gui):
    gui.close()
    logger.info("finish API")

def main():    
    cam_ctrl = Cameras_control()    
    smoke_det = SmokeDetector()
    conf, max_det, det_per_request,weights,ip_server, motion, motion_sensi, show_gui = check_config_file()
    smoke_det.init(weights=weights ,conf_thres=conf, max_det=max_det, motion=motion, motion_sensi= motion_sensi) 
    smoke_det.load_model()
    start_flask(ip_server)    
    logger.info("App on")  
    if show_gui:        
        gui = GUI()
    while True:     
        if not show_gui:
            sleep(1)
            continue
        else:
            sleep(0.1)
            event, _ = gui.window.read(timeout=1)
            if gui.check_close(event): break
            if smoke_det.isloaded: gui.model_online(cam_ctrl.cameras_conected)  
            else: gui.model_offline()       
    finish_app(gui) if show_gui else False

if __name__ == "__main__":    
    try:        
        main()
    except Exception as e :
        logger.error("{}", str(e))
