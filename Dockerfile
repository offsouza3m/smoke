FROM ubuntu:20.04


RUN apt update && apt upgrade -y &&  apt install python3-pip -y
RUN apt install -y build-essential libssl-dev libffi-dev python3-dev
RUN DEBIAN_FRONTEND="noninteractive" TZ="America/Sao_Paulo" apt-get install -y tzdata
RUN apt install ffmpeg libsm6 libxext6  -y
RUN apt install python3-tk -y

RUN mkdir -p /smoke
# Linux
# COPY ./ /smoke  

# win10
COPY . /smoke  
WORKDIR /smoke/

RUN pip3 install -r requirements.txt

EXPOSE 5004

CMD python3 smoke.py


